FROM cloudron/base:0.8.0
MAINTAINER Sourcegraph <support@cloudron.io>

ENV GOROOT /usr/local/go-1.5.1
RUN ln -s /usr/local/go-1.5.1 /usr/local/go
ENV PATH $GOROOT/bin:/usr/local/node-4.2.1/bin:$PATH
ENV GOPATH /home/cloudron/gows

RUN mkdir -p /run/sourcegraph && chown -R cloudron:cloudron /run/sourcegraph

USER cloudron

RUN mkdir -p /home/cloudron/gows/src/src.sourcegraph.com && \
    cd /home/cloudron/gows/src/src.sourcegraph.com && \
    git clone https://src.sourcegraph.com/sourcegraph && \
    cd /home/cloudron/gows/src/src.sourcegraph.com/sourcegraph && \
    make dep && \
    make dist && \
    cp /home/cloudron/gows/src/src.sourcegraph.com/sourcegraph/release/snapshot/linux-amd64 /home/cloudron/sourcegraph && \
    rm -rf /home/cloudron/gows/src

# TODO: java requires gradle, python requires pip
RUN /home/cloudron/sourcegraph toolchain install go javascript

ADD config.ini.template /home/cloudron/config.ini.template
ADD start.sh /home/cloudron/start.sh

RUN rm -rf /home/cloudron/.sourcegraph && ln -s /app/data /home/cloudron/.sourcegraph

CMD [ "/home/cloudron/start.sh" ]


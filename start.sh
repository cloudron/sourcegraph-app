#!/bin/bash

set -eu

sed -e "s,##APP_URL,${APP_ORIGIN}," \
    -e "s/##SSH_PORT/${SSH_PORT}/" \
    -e "s/##LDAP_SERVER/${LDAP_SERVER}/" \
    -e "s/##LDAP_PORT/${LDAP_PORT}/" \
    -e "s/##LDAP_BIND_DN/${LDAP_BIND_DN}/" \
    -e "s/##LDAP_BIND_PASSWORD/${LDAP_BIND_PASSWORD}/" \
    -e "s/##LDAP_USERS_BASE_DN/${LDAP_USERS_BASE_DN}/" \
    /home/cloudron/config.ini.template > /run/sourcegraph/config.ini

chown -R cloudron:cloudron /app/data

exec /home/cloudron/sourcegraph --config=/run/sourcegraph/config.ini serve

